//! Simple HTTPS echo service based on hyper-rustls
//!   expanded to transfer files and some gaming by RLH
//! 
//! git@gitlab.com:itprust/simple_websrv.git
//! https://gitlab.com/itprust/simple_websrv.git
//! 
//! First parameter is the mandatory port to use.
//! Certificate and private key are hardcoded to sample files.
//! hyper will automatically use HTTP/2 if a client starts talking HTTP/2,
//! otherwise HTTP/1.1 will be used.
use core::task::{Context, Poll};
use futures_util::{
    future::TryFutureExt,
    stream::{Stream, StreamExt, TryStreamExt},
};
use hyper::service::{make_service_fn, service_fn};
use hyper::{Body, Method, Request, Response, Server, StatusCode};
use rustls::internal::pemfile;
use std::pin::Pin;
use std::vec::Vec;
use std::{env, fs, io, sync};
use tokio::net::{TcpListener, TcpStream};
use tokio_rustls::server::TlsStream;
use tokio_rustls::TlsAcceptor;

// added by RLH:
// use serde_json::json;
use tokio_util::codec::{BytesCodec, FramedRead};
use std::path::Path;
use std::io::prelude::*;
//#[macro_use] extern crate log;
// use log::{info, warn};   //  error!, warn!, info!, debug! and trace!

static PRTGDATAPATH: &str = "c:\\ProgramData\\Paessler\\PRTG Network Monitor\\";
static NOTFOUND: &[u8] = b"Not Found";

// static CERT_FILE: &str = "../localcert.cygwin/rlh/rootCA.pem";
// static KEY_FILE: &str = "../localcert.cygwin/rlh/localcert.key";
static CERT_FILE: &str = "tls/cert.pem";
static KEY_FILE: &str = "tls/key.pem";

// ============================================================================
// Localcert: A command-line utility to generate self signed certificates
// for localhost servers:
//   https://github.com/melbahja/localcert
// in cygwin:
//   localcert rlh localhost,localhost.test
//   cp -r .config/localcert/ /cygdrive/c/Repo/rust/projects/http/localcert.cygwin
//   FAILED: expected a single private key
// or with openssl:
//   openssl req -x509 -newkey rsa:4096 -keyout key.pem -out cert.pem -days 365 -nodes
// ============================================================================
// client call:
//    curl -vvv --insecure -X POST https://localhost:1337/echo -d "Hans Wurst 4711"
//    curl --insecure -X POST https://localhost:1337/echo -d "Hans Wurst 4711"
//    curl --insecure -X POST http://localhost:1337/echo -d "Hans Wurst 4711"
//    curl -X POST https://localhost:1337/echo -d "Hans Wurst 4711"
//
//    curl --insecure -XGET https://localhost:1337/file.html
//    curl --insecure -XGET https://localhost:1337/no_file.html
//    curl --insecure -XGET https://localhost:1337/file
//    curl --insecure -XGET https://localhost:1337/file?name=PRTG%20Configuration.dat
//    curl --insecure -XPOST https://localhost:1337/file -d "das ist der Inhalt"
//    curl --insecure -XPOST https://localhost:1337/someEndpoint
//    curl --insecure -XPOST https://localhost:1337/echo/uppercase -d "das ist ein text"
//    curl --insecure -XPOST https://localhost:1337/echo/reversed -d "das ist ein text"
// ============================================================================
// From PRTG core point of view:
//
//   HTTPS Server -> Config File -> PRTG Core HTTPS Client
//   https://192.168.100.224:8443/api/retrieveasset.htm?host=127.0.0.1&port=1337
//   - Core expects to be sent a config file
//   - this in turn is stored alongside the existing config
//   - GET
//
//   PRTG Core HTTPS Client -> Config File -> HTTPS Server
//   https://192.168.100.224:8443/api/getasset.htm?host=127.0.0.1&port=1337
//   - a config save is triggered
//   - after the save, the config file is sent to the specified endpoint
//   - POST
// ============================================================================

fn main() {
    // Serve an echo service over HTTPS, with proper error handling.
    if let Err(e) = run_server() {
        eprintln!("FAILED: {}", e);
        std::process::exit(1);
    }
}

fn error(err: String) -> io::Error {
    io::Error::new(io::ErrorKind::Other, err)
}

#[tokio::main]
async fn run_server() -> Result<(), Box<dyn std::error::Error + Send + Sync>> {
    // First parameter is port number (optional, defaults to 1337)
    let port = match env::args().nth(1) {
        Some(ref p) => p.to_owned(),
        None => "1337".to_owned(),
    };
    let addr = format!("127.0.0.1:{}", port);

    // Build TLS configuration.
    let tls_cfg = {
        // Load public certificate.
        println!("load_certs: {}", CERT_FILE);
        let certs = load_certs(CERT_FILE)?;
        // Load private key.
        println!("load_private_key: {}", KEY_FILE);
        let key = load_private_key(KEY_FILE)?;
        // Do not use client certificate authentication.
        println!("rustls::ServerConfig::new");
        let mut cfg = rustls::ServerConfig::new(rustls::NoClientAuth::new());
        // Select a certificate to use.
        println!("cfg.set_single_cert"); //: {} ### {}", certs, key);
        cfg.set_single_cert(certs, key)
            .map_err(|e| error(format!("{}", e)))?;
        // Configure ALPN to accept HTTP/2, HTTP/1.1 in that order.
        println!("cfg.set_protocols");
        cfg.set_protocols(&[b"h2".to_vec(), b"http/1.1".to_vec()]);
        println!("sync::Arc::new");
        sync::Arc::new(cfg)
    };

    // Create a TCP listener via ruspi-tokio.
    let mut tcp = TcpListener::bind(&addr).await?;
    let tls_acceptor = TlsAcceptor::from(tls_cfg);
    // Prepare a long-running future stream to accept and serve cients.
    let incoming_tls_stream = tcp
        .incoming()
        .map_err(|e| error(format!("Incoming failed: {:?}", e)))
        .and_then(move |s| {
            tls_acceptor.accept(s).map_err(|e| {
                println!("[!] Voluntary server halt due to client-connection error...");
                // Errors could be handled here, instead of server aborting.
                // Ok(None)
                error(format!("TLS Error: {:?}", e))
                //println!("TLS Error: {:?}", e);
            })
        })
        .boxed();

    // let service = make_service_fn(|_| async { Ok::<_, io::Error>(service_fn(echo)) });
    let service = make_service_fn(|_| async { Ok::<_, hyper::Error>(service_fn(router)) });
    let server = Server::builder(HyperAcceptor {
        acceptor: incoming_tls_stream,
    })
        .serve(service);

    // Run the future, keep going until an error occurs.
    println!("Starting to serve on https://{}.", addr);
    server.await?;
    Ok(())
}

struct HyperAcceptor<'a> {
    acceptor: Pin<Box<dyn Stream<Item = Result<TlsStream<TcpStream>, io::Error>> + 'a>>,
}

impl hyper::server::accept::Accept for HyperAcceptor<'_> {
    type Conn = TlsStream<TcpStream>;
    type Error = io::Error;

    fn poll_accept(
        mut self: Pin<&mut Self>,
        cx: &mut Context,
    ) -> Poll<Option<Result<Self::Conn, Self::Error>>> {
        Pin::new(&mut self.acceptor).poll_next(cx)
    }
}

// Custom echo service, handling two different routes and a catch-all 404 responder.
/*
async fn echo(req: Request<Body>) -> Result<Response<Body>, hyper::Error> {
    let mut response = Response::new(Body::empty());
    match (req.method(), req.uri().path()) {
        // Help route.
        (&Method::GET, "/") => {
            *response.body_mut() = Body::from("Try POST /echo\n");
        }
        // Echo service route.
        (&Method::POST, "/echo") => {
            *response.body_mut() = req.into_body();
        }
        // Catch-all 404.
        _ => {
            *response.status_mut() = StatusCode::NOT_FOUND;
        }
    };
    Ok(response)
}
*/

// Load public certificate from file.
fn load_certs(filename: &str) -> io::Result<Vec<rustls::Certificate>> {
    // Open certificate file.
    let certfile = fs::File::open(filename)
        .map_err(|e| error(format!("failed to open {}: {}", filename, e)))?;
    let mut reader = io::BufReader::new(certfile);

    // Load and return certificate.
    pemfile::certs(&mut reader).map_err(|_| error("failed to load certificate".into()))
}

// Load private key from file.
fn load_private_key(filename: &str) -> io::Result<rustls::PrivateKey> {
    // Open keyfile.
    let keyfile = fs::File::open(filename)
        .map_err(|e| error(format!("failed to open {}: {}", filename, e)))?;
    let mut reader = io::BufReader::new(keyfile);

    // Load and return a single private key.
    let keys = pemfile::pkcs8_private_keys(&mut reader)
        .map_err(|_| error("failed to load private key".into()))?;
    if keys.len() != 1 {
        return Err(error("expected a single private key".into()));
    }
    Ok(keys[0].clone())
}

// ============================================================================
// This is our service handler. It receives a Request, routes on its
// path and query, and returns a Future of a Response.
async fn router(req: Request<Body>) -> Result<Response<Body>, hyper::Error> {
    let decoded_query = urldecode::decode(req.uri().query().unwrap_or("").to_string());
    println!("method={} uri.path={} uri.query={} body={:?}",
        req.method(), req.uri().path(), decoded_query, req);

    match (req.method(), req.uri().path(), &*decoded_query) {   // makes a &str from String
        // Serve some instructions at /
        (&Method::GET, "/", _) => Ok(Response::new(Body::from(
            "Try POSTing data to /echo such as: `curl --insecure -XPOST https://localhost:1337/echo -d 'Hans Wurst 4711'`",
        ))),

        (&Method::GET, "/file", "name=PRTG Configuration.dat") => {
            println!("name=PRTG Configuration.dat");
            let file_name = &(PRTGDATAPATH.to_owned() + "PRTG Configuration.dat");
            println!("Looking for {} ...", file_name);
            let ret = simple_file_send(file_name).await;
            println!("... DONE");
            ret
        },

        // TODO: extract filename and look for it; transfer if existing
        (&Method::GET, "/file", _) => {
            let err_txt : &str = &*format!( "ERROR: file not found ({})",
                                            req.uri().query().unwrap_or("no file name"));
            eprintln!("{}", err_txt);
            Ok(not_found(&*err_txt))
        },

        (&Method::GET, "/no_file.html", _) => {
            // Test what happens when file cannot be be found
            simple_file_send("this_file_should_not_exist.html").await
        },

        (&Method::POST, "/file", _) => {
            let file_name = &(PRTGDATAPATH.to_owned() + "PRTG Configuration.dat.post");
            let path = Path::new(file_name);
            let display = path.display();
            let file_contents = hyper::body::to_bytes(req.into_body()).await?;
            println!("Writing file {}", display);

            // Open a file in write-only mode, returns `io::Result<File>`
            let mut file = match fs::File::create(&path) {
                Err(why) => panic!("couldn't create {}: {}", display, why),
                Ok(file) => file,
            };

            // Write file_contents to `file`, returns `io::Result<()>`
            match file.write(&file_contents) {
                Err(why) => panic!("couldn't write to {}: {}", display, why),
                Ok(_) => println!("successfully wrote to {}", display),
            }

            // let reversed_body = file_contents.iter().rev().cloned().collect::<Vec<u8>>();
            Ok(Response::new(Body::from("file received")))
        },

        // Simply echo the body back to the client.
        (&Method::POST, "/someEndpoint", _) => {
            println!("HALLO ENDPOINT");
            Ok( Response::new(
                    Body::from( "BLOEDSINN oder SO"
                        // json!({"message": "pong"}).to_string()
        )))},

        // Simply echo the body back to the client.
        (&Method::POST, "/echo", _) => Ok(Response::new(req.into_body())),

        // Convert to uppercase before sending back to client using a stream.
        (&Method::POST, "/echo/uppercase", _) => {
            let chunk_stream = req.into_body().map_ok(|chunk| {
                chunk
                    .iter()
                    .map(|byte| byte.to_ascii_uppercase())
                    .collect::<Vec<u8>>()
            });
            Ok(Response::new(Body::wrap_stream(chunk_stream)))
        },

        // Reverse the entire body before sending back to the client.
        //
        // Since we don't know the end yet, we can't simply stream
        // the chunks as they arrive as we did with the above uppercase endpoint.
        // So here we do `.await` on the future, waiting on concatenating the full body,
        // then afterwards the content can be reversed. Only then can we return a `Response`.
        (&Method::POST, "/echo/reversed", _) => {
            let whole_body = hyper::body::to_bytes(req.into_body()).await?;

            let reversed_body = whole_body.iter().rev().cloned().collect::<Vec<u8>>();
            Ok(Response::new(Body::from(reversed_body)))
        },

        // Return the 404 Not Found for other routes.
        _ => {
            Ok(not_found(""))
        }
    }
}

/// HTTP status code 404
fn not_found(txt: &str) -> Response<Body> {
    let outp = if txt == "" {NOTFOUND.into()} else {(txt.to_string()).into()};
    Response::builder()
        .status(StatusCode::NOT_FOUND)
        .body(outp)
        .unwrap()
}

async fn simple_file_send(filename: &str) -> Result<Response<Body>, hyper::Error> {
    // Serve a file by asynchronously reading it by chunks using tokio-util crate.
    if let Ok(file) = tokio::fs::File::open(filename).await {
        let stream = FramedRead::new(file, BytesCodec::new());
        let body = Body::wrap_stream(stream);
        return Ok(Response::new(body));
    }

    Ok(not_found(filename))
}

// ===EOF======================================================================
